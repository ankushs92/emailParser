import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Ankush on 01/11/16.
 */
public class EmailParser {
    //ENTER THE PATH OF SRC HERE.
    private static final String SRC_FILE_LOCATION = "/Users/Ankush/Desktop/software/Toledo1.txt";

    //ENTER THE PATH WHERE YOU WANT THE EMAILS FILE TO BE.
    private static final String DEST_FILE_LOCATION = "/Users/Ankush/Desktop/software/result.csv";


    public static void main(String[] args) throws IOException {
       //Since the file is not large, we parse it in its entirety and pack the contents into a List.
       final List<String> emails =
                 Files
                .readAllLines(Paths.get(SRC_FILE_LOCATION))
                .stream()
                .map( line ->{
                    //Parse every line, split it by using tab delimiter .The second element of the array is email.
                    final String email = line.split("\\t")[1] + ",";
                    return email;
                })
               .collect(Collectors.toList())
               ;

        //We just spit out this list into
        final File dest = new File(DEST_FILE_LOCATION);

        Files.write(Paths.get(DEST_FILE_LOCATION), emails, Charset.defaultCharset());
    }
}
